import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hearder',
  templateUrl: './hearder.component.html',
  styleUrls: ['./hearder.component.scss']
})
export class HearderComponent implements OnInit {

  appTitle:string = 'Awadhesh Singh'

  constructor() { }

  ngOnInit() {
  }

}
